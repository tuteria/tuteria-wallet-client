#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = '0.0.2'

setup(
    name='Tuteria',
    version=version,
    author='',
    author_email='gbozee@gmail.com',
    packages=[
        'tuteria_wallet_client',
    ],
    include_package_data=True,
    install_requires=[
        'requests',
    ],
    zip_safe=False,
)