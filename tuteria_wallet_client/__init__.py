import os
import json
import requests

class TuteriaWalletClient:
    base_url = os.getenv('WALLET_MICROSERVICE_URL','http://localhost:8002')

    def update_user_detail(self, user):
        """
        updates the user detail in the wallet microservice
        """
        data = json.dumps({
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'username': user.username,
            'is_staff': user.is_staff,
            'is_superuser': user.is_superuser
        })

        response = requests.post(self.base_url+'/users/create/',
            data=data)
        response.raise_for_status()
        return response.json()